﻿using System;
using ConsoleApp1;

namespace Client
{
    class Client
    {
        static void Main(string[] args)
        {
            Stringer_1 obj = new Stringer_1();
            obj.InfoStringer();

            // Здесь будет загружаться модуль auto.netmodule
            Stringer obj1 = new Stringer();
            obj1.StringerMethod();
            Console.ReadLine();
        }
    }
}
